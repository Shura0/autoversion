#!/usr/bin/perl -w


my $autostartdir=`kde4-config --path autostart`;
$autostartdir=~s/:.*//;
chomp $autostartdir;
my $servicedir=`kde4-config --path services`;
$servicedir=~s/:.*//;
chomp $servicedir;
print "Deleting files and folders...\n";
unlink "$servicedir/ServiceMenus/ver_directories.desktop" or print "can't delete $servicedir/ServiceMenus/ver_directories.desktop\n";
unlink "$servicedir/ServiceMenus/ver_files.desktop" or print "can't delete $servicedir/ServiceMenus/ver_files.desktop\n";
rmdir "$servicedir/ServiceMenus/autobackup-scripts" or print "can't remove directory $servicedir/ServiceMenus/autobackup-scripts: $!\n";
unlink "$servicedir/ServiceMenus/autobackup-scripts/autobackup.pl" or print "can't delete $servicedir/ServiceMenus/autobackup-scripts/autobackup.pl\n";
unlink "$servicedir/ServiceMenus/autobackup-scripts/dirwatcher.pl" or print "can't delete $servicedir/ServiceMenus/autobackup-scripts/dirwatcher.pl\n";
unlink "$autostartdir/dirwatcher.desktop" or print "Can't delete $autostartdir/dirwatcher.desktop\n";
print "Deinstallation complete\n";
print "Stopping dirwatcher daemon...";
`killall dirwatcher.pl 2>/dev/null`;
if (not $?)
{
  print "ok\n";
}
else
{
  print "fail\n";
}