#!/usr/bin/perl -w

#use strict;
use Archive::Zip;
use File::Basename;
use File::Find;
use Sys::Syslog qw( :DEFAULT setlogsock);

$|=1;
my %DIRS;
my $RUN;
my $BACKUPDIRECTORY=".autobackup";
my %FILES;
my @FINDFILES;
my $configfile="autobackuprc";
my $path=`kde4-config --path config` or die "cannot find kde4-config\n";
my @path=split /:/, $path;

$configfile=$path[0].$configfile;
openlog($0,'','user');


# double run protection
my $check=`ps -A | grep [d]irwatcher.pl`;
my @check=split /\n/,$check;
if(@check>1)
{
  syslog('info','daemon already started');
  exit 0;
}
# CONFIG FORMAT IS:
# [directory]={*.doc,*.xls}
sub read_config
{
  #$SIG{HUP} = \&read_config;
  syslog('info', "HUP signal");
  open my $cfg, "<$configfile" or return "Config not found";
  %DIRS=();
  while(<$cfg>)
  {
		if(/\[(.*)\]={(.*)}$/)
		{
			$DIRS{$1}=$2;
		}
  }
  close $cfg;
	read_dirs();
  return "Config opened successful";
	#reread dirs

}
$SIG{HUP}=\&read_config;

sub read_dirs
{
  my @files;
  foreach my $i (keys %DIRS)
  {
		my @tmp=split ',', $DIRS{$i};
		@tmp=map{"$i/".$_} @tmp;
		foreach(@tmp)
		{
			push @files,glob;
		}
		while (my $file=shift @files)
		{
			my @arr=stat($file);
			$FILES{$file}=$arr[9];
		}
  }
}

sub found
{
  push @FINDFILES,$_;
}

sub check_dirs
{
  my @files;
  foreach my $i (keys %DIRS)
  {
	#$DIRS{$i}=~s/[\{\}]//;
		my @tmp=split ',', $DIRS{$i};
		@tmp=map{"$i/".$_} @tmp;
		foreach (@tmp)
		{
			s/ /\\ /;
			my @t=glob;
			push @files,@t if @t;
		}
		while (my $file=shift @files)
		{
			my @arr=stat($file);
			if(not $FILES{$file} or ($FILES{$file} != $arr[9]))
			{
				$FILES{$file}=$arr[9];
				@FINDFILES=();
				find({wanted=>\&found, no_chdir=>1,follow=>0}, "$i/.autobackup/");
				my $f=basename $file;
				my @f=grep /$f[_][^_]+\.zip/,@FINDFILES;
				@f=sort {my @a=stat($a);my @b=stat($b);$a[10]<=>$b[10]} @f; # sort by creation time
				my $rm=shift @f;
				unlink $rm or syslog('info', "Impossible to remove $rm\n") if(@f>100); # maximum verion reached!
				zip($file);
				syslog('info', "file $file was modified!");
				#`kdialog --title Versions--passivepopup "File $file changed" 2`
			}
		}
  }
}


sub zip
{
  my $filename=shift;
  my $obj = Archive::Zip->new();
  $obj->addFile($filename);   # add files
  my $date=localtime();
  my $basename=basename($filename);
  my $dir=dirname($filename);
  $obj->writeToFileNamed("$dir/$BACKUPDIRECTORY/$basename"."_$date.zip");
  #print "$dir/$BACKUPDIRECTORY/$basename"."_$date.zip\n";
}

sub main
{
  local $SIG{HUP}=\&read_config;
  local $SIG{TERM}=sub{$RUN=0};
  while ($RUN)
  {
		check_dirs();
		sleep 5;
  }
}

sub daemonize {
   use POSIX;
   POSIX::setsid or die "setsid: $!";
   my $pid = fork ();
   if ($pid < 0) {
      die "fork: $!";
   } elsif ($pid) {
      exit 0;
   }
   chdir "/";
   umask 0;
   foreach (0 .. (POSIX::sysconf (&POSIX::_SC_OPEN_MAX) || 1024))
      { POSIX::close $_ }
   open (STDIN, "</dev/null");
   open (STDOUT, ">/dev/null");
   open (STDERR, ">&STDOUT");
 }


# ========= MAIN ==========
$RUN=1;
daemonize();
syslog('info',read_config());
main();
closelog();