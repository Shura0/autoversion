#!/usr/bin/perl -w
#
#     Copyright 2011 Alexander Zaitsev <shura0@yandex.ru>
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, see http://www.gnu.org/licenses
#     or write to the Free Software Foundation,Inc., 51 Franklin Street,
#     Fifth Floor, Boston, MA 02110-1301  USA

use File::Basename;
use File::Copy;
use File::Find;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
my $configfile="autobackuprc";
my $path=`kde4-config --path config` or die "cannot find kde4-config\n";
my @path=split /:/, $path;
$configfile=$path[0].$configfile;
my %DIRS;
my @FILES=();
my $TEMPLATE="*.txt,*.doc,*.xls,*.ppt,*.odt,*.ods,*.odp,*.pdf";

sub read_config
{
  open $cfg, "<$configfile" or return "Config not found\n";
  %DIRS=();
  while(<$cfg>)
  {
	if(/\[(.*)\]=(.*)$/)
	{
	  $DIRS{$1}=$2;
	}
  }
  close $cfg;
  return "Config opened successful\n";
}

sub write_config
{
  open $cfg, ">$configfile" or return "Can't write config\n";
  foreach my $i (keys %DIRS)
  {
	print $cfg "[$i]=".$DIRS{$i}."\n";
  }
  close $cfg;
  `killall -HUP dirwatcher.pl`;
  return "Config writed successful\n";
}

read_config();
my $do=shift;
if ($do eq "add")
{
  while(my $folder=shift)
  {
		add($folder);
	}
		write_config();
}elsif($do eq "del")
{
  while (my $folder=shift)
	{
		del($folder);
	}
	write_config();
}elsif($do eq "rev")
{
  $folder=shift;
  get_rev($folder);
}

sub add
{
  my $dir=shift;
	#{
		if($DIRS{$dir})
		{
			`kdialog --title Versions --icon "dialog-error" --passivepopup "Monitoring of $dir is already enabled!" 5`;
			return;
		}
		$TEMPLATE="*.txt,*.doc,*.xls,*.ppt,*.odt,*.ods,*.odp,*.pdf" if not $TEMPLATE;
		$TEMPLATE=`kdialog --title "Enable monitoring" --inputbox "Folder: $dir\nWhat files do you want to monitor?" "$TEMPLATE"`;
		chomp $TEMPLATE;
		return if not $TEMPLATE;
		$DIRS{$dir}="{".$TEMPLATE."}";
		mkdir "$dir/.autobackup" if not -d "$dir/.autobackup";
	#}
}

sub del
{
  my $dir=shift;
	if(defined $DIRS{$dir})
	{
		delete $DIRS{$dir};
		`kdialog --title Versions--passivepopup "Monitoring of $dir is stopped!" 5`;
	}
	else
	{
		`kdialog --passivepopup "Monitoring of $dir is not enabled!" 5 --icon "dialog-error" --title Versions`;
	}
}


sub found
{
	push @FILES,$_;
}
sub get_rev
{
  my $file=shift;
  my $dir=dirname($file);
  my $filename=basename($file);
  return if not -d "$dir/.autobackup";
  @FILES=();
  find({wanted=>\&found, no_chdir=>1,follow=>0}, "$dir/.autobackup/");
  my @ufiles=grep(/$filename[_]/,@FILES);
  my @files=reverse sort {my @a=stat($a);my @b=stat($b);$a[10]<=>$b[10]} @ufiles;
  my @dates;
  foreach (@files)
  {
	if(/_([^_]*)\.zip/)
	{
	  push @dates,$1;
	}
  }
  my $res=join "\" \"",@dates;
  $res="\"$res\"";
  my $ver=`kdialog --combobox "Restore:" $res --title "Revisions"`;
  chomp $ver;
  return if not $ver;
  foreach (@files)
  {
	my $file=$_;
	if(/$ver/)
	{
	  #`kdialog --passivepopup "$_ restored" 5`;
	  if(/(.*)_/)
	  {
		my $originalname=$1;
		my $dir=dirname($originalname);
		my $filename=basename($originalname);
		
		#print "Zip: $file to $dir\n";
		my $zip=Archive::Zip->new("$file");
		$dir=~s/.autobackup//;
		if($zip->extractTree($dir,$dir) !=AZ_OK)
		{
		  #print "Can't restore\n";
		  `kdialog --passivepopup "$filename cannot be restored" 5 --title Versions`;
		  
		}
		else
		{
			`kdialog --passivepopup "$filename restored" 5 --title Versions`;
		}
	  }
	  last;
	}
  }
}
