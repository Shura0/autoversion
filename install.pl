#!/usr/bin/perl -w

use File::Find;
use File::Copy;
use File::Path qw(make_path);
use Test::More tests=>1;
require_ok(Archive::Zip);

my $autostartdir=`kde4-config --path autostart`;
$autostartdir=~s/:.*//;
chomp $autostartdir;
my $servicedir=`kde4-config --path services`;
$servicedir=~s/:.*//;
chomp $servicedir;


make_path "$servicedir/ServiceMenus/autobackup-scripts" if not -d "$servicedir/ServiceMenus/autobackup-scripts";
copy('ver_directories.desktop',"$servicedir/ServiceMenus") or die "Cannot copy 'ver_directories.desktop' to $servicedir/ServiceMenus";
copy('ver_files.desktop',"$servicedir/ServiceMenus") or die "Cannot copy 'ver_files.desktop' to $servicedir/ServiceMenus";
copy('autobackup.pl', "$servicedir/ServiceMenus/autobackup-scripts") or die "Cannot copy 'autobackup.pl' to $servicedir/ServiceMenus/autobackup-scripts";
`chmod +x $servicedir/ServiceMenus/autobackup-scripts/autobackup.pl`;
copy('dirwatcher.pl', "$servicedir/ServiceMenus/autobackup-scripts") or die"Cannot copy 'dirwatcher.pl' to $servicedir/ServiceMenus/autobackup-scripts";
`chmod +x $servicedir/ServiceMenus/autobackup-scripts/dirwatcher.pl`;
open FILEIN, "<dirwatcher.desktop" or die "Cannot open dirwatcher.desktop";
open FILEOUT, ">$autostartdir/dirwatcher.desktop" or die "Cannot write $autostartdir/dirwatcher.desktop";
while (<FILEIN>)
{
  s/\$SERVICES/$servicedir/;
  print FILEOUT;
}
close FILEIN;
close FILEOUT;
print "Installation complete\n";
print "Stopping dirwatcher daemon...";
`killall dirwatcher.pl 2>/dev/null`;
if (not $?)
{
  print "ok\n";
}
else
{
  print "fail\n";
}
print "Running dirwatcher daemon...";
if(not system("$servicedir/ServiceMenus/autobackup-scripts/dirwatcher.pl"))
{
  print "ok\n";
}
else
{
  print "something went wrong!\n";
}